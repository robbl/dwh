USE [dwhprak01];

IF OBJECT_ID('dbo.view_acm_citings_with_self_citing') IS NOT NULL 
    DROP VIEW [dbo].[view_acm_citings_with_self_citing]

IF OBJECT_ID('dbo.view_acm_publications_with_self_citing') IS NOT NULL 
    DROP VIEW [dbo].[view_acm_publications_with_self_citing]

GO

-- view for citings including self citing flag
CREATE VIEW dbo.view_acm_citings_with_self_citing
AS
    SELECT
        citing_publication_id, cited_publication_id, text
       ,(
        CASE
        WHEN SUM(ctemp.author_included_in_citing_text) > 0
        THEN 1
        ELSE 0
        END
       ) AS self_citing
    FROM
    (
        SELECT
            c.*
           ,(
                CASE
                WHEN PATINDEX('%' + a.name + '%' , c.text) > 0
                THEN 1
                ELSE 0
                END
            ) AS author_included_in_citing_text
        FROM
            dbo.acm_publications AS p
        JOIN
            dbo.acm_author_publication_maps AS apm
            ON p.id = apm.publication_id
        JOIN
            dbo.acm_authors AS a
            ON apm.author_id = a.id
        JOIN
            dbo.acm_citings AS c
            ON p.id = c.citing_publication_id
    ) AS ctemp
    GROUP BY citing_publication_id, cited_publication_id, text

GO

-- view for publications including self citing flag
CREATE VIEW dbo.view_acm_publications_with_self_citing
AS
    SELECT
        p.id
       ,p.end_page
       ,p.start_page
       ,p.title
       ,MAX(c.self_citing) AS self_citing
    FROM
        dbo.acm_publications AS p
    LEFT JOIN
        dbo.view_acm_citings_with_self_citing AS c
        ON p.id = c.citing_publication_id
    GROUP BY p.id, p.end_page, p.start_page, p.title

GO

-- all authors with a least on self citing publication
SELECT
    a.*
   ,author_id_with_count_self_citing_publications.count_self_citing_publications
FROM
    (
        -- all authors with their count of self citing publications
        SELECT
            a.id
           ,COUNT(p.id) AS count_self_citing_publications
        FROM
            dbo.acm_authors AS a
        JOIN
            dbo.acm_author_publication_maps AS apm
            ON a.id = apm.author_id
        JOIN
            dbo.view_acm_publications_with_self_citing AS p
            ON apm.publication_id = p.id
        WHERE p.self_citing = 1 -- only count publications with self citings
        GROUP BY a.id
        
    ) AS author_id_with_count_self_citing_publications
JOIN
    dbo.acm_authors AS a
    ON author_id_with_count_self_citing_publications.id = a.id
ORDER BY author_id_with_count_self_citing_publications.count_self_citing_publications DESC