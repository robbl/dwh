USE [dwhprak01];

--> clear all tables

IF OBJECT_ID('dbo.cube_author_publication_maps', 'U') IS NOT NULL
  DELETE FROM [dbo].[cube_publications];

IF OBJECT_ID('dbo.cube_publications', 'U') IS NOT NULL
  DELETE FROM [dbo].[cube_publications];

IF OBJECT_ID('dbo.cube_authors', 'U') IS NOT NULL
  DELETE FROM [dbo].[cube_authors];

IF OBJECT_ID('dbo.cube_times', 'U') IS NOT NULL
  DELETE FROM [dbo].[cube_times];

IF OBJECT_ID('dbo.cube_titles', 'U') IS NOT NULL
  DELETE FROM [dbo].[cube_titles];

IF OBJECT_ID('dbo.cube_venue_series', 'U') IS NOT NULL
  DELETE FROM [dbo].[cube_venue_series];

-- insert all times
INSERT INTO 
    [dbo].[cube_authors]
    (
        id
       ,name
       ,institution
    )
    SELECT
        id
       ,name
       ,''
    FROM 
        [dbo].[dblp_authors] AS dblp_authors
    LEFT JOIN
        [dbo].[view_acm_authors_with_single_institution] AS acm_awsi
        ON dblp_authors.name = acm_awsi.acm_author_name
;

-- insert all times
INSERT INTO 
    [dbo].[cube_times]
    (
        year
       ,decade
    )
    SELECT DISTINCT
        year
       ,(year - (year % 10))
    FROM 
        [dbo].[dblp_publications]
;

-- insert all titles
INSERT INTO 
    [dbo].[cube_titles]
    (
        id
       ,title
    )
    SELECT
        id
       ,title
    FROM 
        [dbo].[dblp_publications]
;

-- insert all venue series
INSERT INTO 
    [dbo].[cube_venue_series]
    (
        id
       ,description
    )
    SELECT
        id
       ,name
    FROM 
        [dbo].[dblp_venue_series]
;

-- insert all publications
INSERT INTO cube_publications
    (
        title_id
       ,time_year
       ,venue_series_id
       ,citings_gs
       ,citings_acm
       ,citings_acm_self
       
    )
    SELECT
	    dblp_p.id AS title_id
	   ,dblp_p.year AS year	
	   ,dblp_v.venue_series_id AS venue_series_id
	   ,CASE WHEN dblp_pwgc.gs_citings_count IS NULL 
            THEN 0
            ELSE dblp_pwgc.gs_citings_count
        END AS citings_acm
	   ,CASE WHEN dblp_pwac.acm_citings_count IS NULL 
            THEN 0
            ELSE dblp_pwac.acm_citings_count
        END AS citings_acm
	   ,CASE WHEN dblp_pwacs.acm_citings_self_count IS NULL 
            THEN 0
            ELSE dblp_pwacs.acm_citings_self_count
        END AS citings_acm
    FROM
	    dbo.dblp_publications AS dblp_p
    LEFT JOIN
	    dbo.dblp_publication_venue_maps AS dblp_pvm
        ON dblp_p.id = dblp_pvm.publication_id
    LEFT JOIN
	    dbo.dblp_venues AS dblp_v
        ON dblp_pvm.venue_id = dblp_v.id
    LEFT JOIN
        dbo.view_dblp_publications_with_gs_citings AS dblp_pwgc
        ON dblp_p.id = dblp_pwgc.dblp_publication_id
    LEFT JOIN
        dbo.view_dblp_publications_with_acm_citings AS dblp_pwac
        ON dblp_p.id = dblp_pwac.dblp_publication_id
    LEFT JOIN
        dbo.view_dblp_publications_with_acm_citings_self AS dblp_pwacs
        ON dblp_p.id = dblp_pwacs.dblp_publication_id
;

-- insert all author publication maps
INSERT INTO 
    [dbo].[cube_author_publication_maps]
    (
        author_id
       ,publication_id
    )
    SELECT DISTINCT
        author_id
       ,id AS publication_id
    FROM
        dbo.dblp_author_publication_maps AS dblp_apm
    LEFT JOIN 
        dbo.cube_publications AS cube_p
        ON dblp_apm.publication_id = cube_p.title_id
;
