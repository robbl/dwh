USE [dwhprak01];

-- drop views

IF OBJECT_ID('dbo.view_acm_authors_with_single_institution') IS NOT NULL 
    DROP VIEW [dbo].[view_acm_authors_with_single_institution];

IF OBJECT_ID('dbo.view_dblp_publications_with_gs_citings') IS NOT NULL 
    DROP VIEW [dbo].[view_dblp_publications_with_gs_citings];

IF OBJECT_ID('dbo.view_dblp_publications_with_acm_citings') IS NOT NULL 
    DROP VIEW [dbo].[view_dblp_publications_with_acm_citings];
    
IF OBJECT_ID('dbo.view_dblp_publications_with_acm_citings_self') IS NOT NULL 
    DROP VIEW [dbo].[view_dblp_publications_with_acm_citings_self];

-- drop mapping tables

IF OBJECT_ID('dbo.cube_author_publication_maps', 'U') IS NOT NULL
    DROP TABLE [cube_author_publication_maps];

-- drop tables

IF OBJECT_ID('dbo.cube_authors', 'U') IS NOT NULL
    DROP TABLE [cube_authors];

IF OBJECT_ID('dbo.cube_publications', 'U') IS NOT NULL
    DROP TABLE [cube_publications];

IF OBJECT_ID('dbo.cube_titles', 'U') IS NOT NULL
    DROP TABLE [cube_titles];

IF OBJECT_ID('dbo.cube_venue_series', 'U') IS NOT NULL
    DROP TABLE [cube_venue_series];

IF OBJECT_ID('dbo.cube_times', 'U') IS NOT NULL
    DROP TABLE [cube_times];

-- create tables

CREATE TABLE [dbo].[cube_authors]
(
    id          bigint
                CONSTRAINT cube_authors_id_nn NOT NULL
                CONSTRAINT cube_authors_pk PRIMARY KEY

   ,name        varchar(255)

   ,institution varchar(255)
);

CREATE TABLE [dbo].[cube_titles]
(
    id      bigint
            CONSTRAINT cube_titles_id_nn NOT NULL
            CONSTRAINT cube_titles_pk PRIMARY KEY

   ,title   varchar(255)
);

CREATE TABLE [dbo].[cube_times]
(
    year    int
            CONSTRAINT cube_times_year_nn NOT NULL
            CONSTRAINT cube_times_pk PRIMARY KEY

   ,decade  int
);

CREATE TABLE [dbo].[cube_venue_series]
(
    id          BIGINT
                CONSTRAINT cube_venue_series_id_nn NOT NULL
                CONSTRAINT cube_venue_series_pk PRIMARY KEY

   ,description varchar(255)
);

CREATE TABLE [dbo].[cube_publications]
(
    id                  bigint
                        IDENTITY
                        CONSTRAINT cube_publications_id_nn NOT NULL
                        CONSTRAINT cube_publications_pk PRIMARY KEY

   ,title_id            bigint
                        CONSTRAINT cube_publications_titles_id_fk 
                            FOREIGN KEY REFERENCES [dbo].[cube_titles] (id)

   ,time_year           int
                        CONSTRAINT cube_publications_times_year_fk 
                            FOREIGN KEY REFERENCES [dbo].[cube_times] (year)

   ,venue_series_id     BIGINT
                        CONSTRAINT cube_publications_venue_series_id_fk 
                            FOREIGN KEY REFERENCES [dbo].[cube_venue_series] (id)

   ,citings_gs          INT 
                        DEFAULT 0

   ,citings_acm         INT 
                        DEFAULT 0

   ,citings_acm_self    INT
                        DEFAULT 0
);

CREATE TABLE [dbo].[cube_author_publication_maps]
(
    author_id       bigint
                    CONSTRAINT cube_author_publication_maps_author_id_nn NOT NULL
                    CONSTRAINT cube_author_publication_maps_author_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[cube_authors] (id)

   ,publication_id  bigint
                    CONSTRAINT cube_author_publication_maps_publication_id_nn NOT NULL
                    CONSTRAINT cube_author_publication_maps_publication_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[cube_publications] (id)

   ,CONSTRAINT cube_author_publication_maps_pk 
        PRIMARY KEY (publication_id, author_id)
);

GO

CREATE VIEW dbo.view_acm_authors_with_single_institution
AS
    SELECT
        acm_authors.name AS acm_author_name
       ,acm_institutions.name AS acm_institution_name
    FROM
        (
            -- select on intituion per author using group by & max
            SELECT 
                author_id
               ,max(new_institution_id) as institution_id
            FROM 
                dbo.view_acm_author_publication_maps_cleaned
            WHERE 
                new_institution_id IS NOT NULL
            GROUP BY
                author_id
        ) AS acm_authors_with_single_institution
    JOIN
        dbo.acm_authors AS acm_authors
        ON acm_authors_with_single_institution.author_id = acm_authors.id 
    JOIN
        dbo.view_acm_institutions_cleaned AS acm_institutions
        ON acm_authors_with_single_institution.institution_id = acm_institutions.id
        
GO

CREATE VIEW dbo.view_dblp_publications_with_gs_citings
AS
    SELECT
        dblp_id AS dblp_publication_id
       ,SUM(no_of_citings) AS gs_citings_count
    FROM
        dbo.dblp_gs_publication_matches AS dg_pm
    LEFT JOIN
        dbo.gs_publications AS gs_p
        ON dg_pm.gs_id = gs_p.id
    GROUP BY dblp_id
;

GO

CREATE VIEW dbo.view_dblp_publications_with_acm_citings
AS
    SELECT
	    dblp_acm.dblp_id AS dblp_publication_id
	   ,COUNT(*) AS acm_citings_count
    FROM
	    dbo.dblp_acm_publication_matches AS dblp_acm
    LEFT JOIN
	    dbo.acm_citings AS acm_citings
        ON dblp_acm.acm_id = acm_citings.cited_publication_id
    GROUP BY
	    dblp_acm.dblp_id
;

GO

CREATE VIEW dbo.view_dblp_publications_with_acm_citings_self
AS
    SELECT
        dblp_id AS dblp_publication_id
       ,self_citings AS acm_citings_self_count
    FROM
        dbo.dblp_acm_publication_matches AS da_pm
    LEFT JOIN
        (
            SELECT
                citing_publication_id
               ,SUM(self_citing) AS self_citings
            FROM
                dbo.view_acm_citings_with_self_citing AS v_acm_cwsc
            GROUP BY v_acm_cwsc.citing_publication_id
        ) AS acm_self_citings_per_publication
        ON da_pm.acm_id = acm_self_citings_per_publication.citing_publication_id
;