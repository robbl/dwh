USE [dwhprak01];

IF OBJECT_ID('dbo.view_gs_publications_with_venue_series') IS NOT NULL 
    DROP VIEW [dbo].[view_gs_publications_with_venue_series]
    
IF OBJECT_ID('dbo.view_gs_publications_sigmod') IS NOT NULL 
    DROP VIEW [dbo].[view_gs_publications_sigmod]
    
IF OBJECT_ID('dbo.view_gs_publications_acmtods') IS NOT NULL 
    DROP VIEW [dbo].[view_gs_publications_acmtods]

IF OBJECT_ID('dbo.view_gs_publications_vldb') IS NOT NULL 
    DROP VIEW [dbo].[view_gs_publications_vldb]

GO

-- get all publications with venue series
CREATE VIEW dbo.view_gs_publications_with_venue_series
AS
    SELECT 
        p.id
       ,p.title
       ,vs.id AS venue_series_id
       ,vs.name AS venue_series_name 
    FROM 
        dbo.gs_publications AS p
    JOIN
        dbo.gs_publication_venue_maps AS pv 
            ON p.id = pv.publication_id
    JOIN
        dbo.gs_venues AS v
            ON pv.venue_id = v.id
    JOIN
        dbo.gs_venue_series AS vs
            ON v.venue_series_id = vs.id

GO

-- get all publications from SIGMOD
CREATE VIEW dbo.view_gs_publications_sigmod
AS
    SELECT 
        id
       ,title
    FROM 
        dbo.view_gs_publications_with_venue_series
    WHERE
        venue_series_name LIKE '%sigmod%'

GO

-- get all publications from ACM ToDS
CREATE VIEW dbo.view_gs_publications_acmtods
AS
    SELECT 
        id
       ,title
    FROM 
        dbo.view_gs_publications_with_venue_series
    WHERE
        venue_series_name LIKE '%tods%'

GO

-- get all publications from VLDB
CREATE VIEW dbo.view_gs_publications_vldb
AS
    SELECT 
        id
       ,title
    FROM 
        dbo.view_gs_publications_with_venue_series
    WHERE
        venue_series_name LIKE '%vldb%'
