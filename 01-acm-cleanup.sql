USE [dwhprak01];

--> drop all temp tables

IF OBJECT_ID('dbo.acm_authors_temp', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_authors_temp];
  
IF OBJECT_ID('dbo.acm_citing_publications_temp', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_citing_publications_temp];

IF OBJECT_ID('dbo.acm_publications_temp', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_publications_temp];

IF OBJECT_ID('dbo.acm_venues_temp', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_venues_temp];

IF OBJECT_ID('dbo.acm_venue_series_temp', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_venue_series_temp];

--< drop all temp tables 

-- drop the view
IF OBJECT_ID('dbo.view_acm_venues', 'V') IS NOT NULL
  DROP VIEW [dbo].[view_acm_venues];
