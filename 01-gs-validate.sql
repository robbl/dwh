USE [dwhprak01];

GO

SELECT
    COUNT(DISTINCT id) AS distinct_p_from_sa
FROM
    [dbo].[gs_publications_splitted_additional]
;

SELECT
    COUNT(DISTINCT id) AS distinct_p
FROM
    [dbo].[gs_publications]
;

SELECT
    COUNT(DISTINCT author_name) AS distinct_a_from_sa
FROM
    [dbo].[gs_publications_splitted_additional]
;


SELECT
    COUNT(DISTINCT name) AS distinct_a
FROM
    [dbo].[gs_authors]
;

SELECT
    COUNT(DISTINCT origin_name) AS distinct_o_from_sa
FROM
    [dbo].[gs_publications_splitted_additional]
;

SELECT
    COUNT(DISTINCT name) AS distinct_o
FROM
    [dbo].[gs_origins]
;

SELECT
    COUNT(DISTINCT id) as distinct_p_without_o_from_sa
FROM
    [dbo].[gs_publications_splitted_additional]
WHERE 
    origin_name IS NULL
;

SELECT
    COUNT(DISTINCT id) as distinct_p_without_o
FROM
    [dbo].[gs_publications]
WHERE 
    origin_id IS NULL
;

SELECT
    COUNT(DISTINCT id) as distinct_p_without_vs_from_sa
FROM
    [dbo].[gs_publications_splitted_additional]
WHERE 
    venue_series_name IS NULL
;

SELECT
    id
   ,additional
FROM
    dbo.gs_publications_import
WHERE
    id = 10835807973923721746
;

SELECT
    p.id
   ,a.name
   ,apm.position
   ,vs.name
   ,v.year
   ,o.name
FROM
    dbo.gs_publications AS p
    LEFT JOIN dbo.gs_author_publication_maps AS apm
        ON p.id = apm.publication_id
    LEFT JOIN dbo.gs_authors AS a
        ON apm.author_id = a.id
    LEFT JOIN dbo.gs_publication_venue_maps AS pvm
        ON p.id = pvm.publication_id
    LEFT JOIN dbo.gs_venues AS v
        ON pvm.venue_id = v.id
    LEFT JOIN dbo.gs_venue_series AS vs
        ON v.venue_series_id = vs.id
    LEFT JOIN dbo.gs_origins AS o
        ON p.origin_id = o.id
WHERE
    p.id = 10835807973923721746
ORDER BY
    apm.position
;