USE [dwhprak01];

-- show new against old institution names
/*
SELECT 
    ino.id
   ,ino.name AS new_name
   ,i.name AS old_name
FROM 
    dbo.view_acm_institutions_normalized AS ino
JOIN 
    dbo.acm_institutions AS i
    ON ino.id = i.id
*/

-- show all institutions that could be the same with regards to publications by the same author in the same year
SELECT
    apim1.institution_id
   ,apim1.institution_name
   ,apim2.institution_id as institution_id2
   ,apim2.institution_name as institution_name2
FROM
    dbo.view_acm_author_publication_institution_mappings AS apim1
JOIN
    dbo.view_acm_author_publication_institution_mappings AS apim2
    ON 
        apim1.author_id = apim2.author_id 
        AND apim1.publication_id <> apim2.publication_id
        AND apim1.institution_id <> apim2.institution_id
        AND apim1.institution_name <> apim2.institution_name
        AND apim1.publication_year = apim2.publication_year
GROUP BY apim1.institution_id, apim1.institution_name, apim2.institution_id, apim2.institution_name
ORDER BY institution_id
