USE [dwhprak01];

--> fn_gs_concat_author_names

/*
    function will concat all authors for a given publication

    input:

        10143649680349990526

    output:

        J Chen, DJ DeWitt, F Tian, Y Wang
*/

IF OBJECT_ID('dbo.fn_gs_concat_author_names') IS NOT NULL
  DROP FUNCTION [dbo].[fn_gs_concat_author_names];

GO

CREATE FUNCTION [dbo].[fn_gs_concat_author_names] 
    (
        @publication_id numeric(20, 0)
    )
RETURNS 
    nvarchar(MAX)
AS
BEGIN
    DECLARE 
      @output       nvarchar(MAX)

    SELECT 
        @output = SUBSTRING(D.items, 1, LEN(D.items) - 1)
    FROM
    (
        SELECT
        (
            SELECT
                author_name + ', '
            FROM
                dbo.gs_publications_splitted_additional
            WHERE
                publication_id = @publication_id
            ORDER BY
                author_position
            FOR XML PATH ('')
           ,type
        
        ).value('(./text())[1]','varchar(max)')
    ) AS D (items);

    RETURN @output
END

--< fn_gs_concat_author_names
