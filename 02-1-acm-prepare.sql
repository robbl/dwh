USE [dwhprak01];

IF OBJECT_ID('dbo.view_acm_publications_with_venue_series') IS NOT NULL 
    DROP VIEW [dbo].[view_acm_publications_with_venue_series]
    
IF OBJECT_ID('dbo.view_acm_publications_sigmod_journal') IS NOT NULL 
    DROP VIEW [dbo].[view_acm_publications_sigmod_journal]
    
IF OBJECT_ID('dbo.view_acm_publications_acmtods_journal') IS NOT NULL 
    DROP VIEW [dbo].[view_acm_publications_acmtods_journal]

IF OBJECT_ID('dbo.view_acm_publications_vldb_journal') IS NOT NULL 
    DROP VIEW [dbo].[view_acm_publications_vldb_journal]

IF OBJECT_ID('dbo.view_acm_publications_vldb_series') IS NOT NULL 
    DROP VIEW [dbo].[view_acm_publications_vldb_series]
   
IF OBJECT_ID('dbo.view_acm_publications_sigmod_series') IS NOT NULL 
    DROP VIEW [dbo].[view_acm_publications_sigmod_series]

GO

-- get all publications with venue series
CREATE VIEW dbo.view_acm_publications_with_venue_series
AS
    SELECT 
        p.id
       ,p.title
       ,vs.id AS venue_series_id 
    FROM 
        dbo.acm_publications AS p
    JOIN
        dbo.acm_publication_venue_maps AS pv 
            ON p.id = pv.publication_id
    JOIN
        dbo.acm_venues AS v
            ON pv.venue_id = v.id
    JOIN
        dbo.acm_venue_series AS vs
            ON v.venue_series_id = vs.id

GO

-- get all publications from SIGMOD (Journal)
CREATE VIEW dbo.view_acm_publications_sigmod_journal
AS
    SELECT 
        id
       ,title
    FROM 
        dbo.view_acm_publications_with_venue_series
    WHERE
        venue_series_id = 6

GO

-- get all publications from ACM ToDS (Journal)
CREATE VIEW dbo.view_acm_publications_acmtods_journal
AS
    SELECT 
        id
       ,title
    FROM 
        dbo.view_acm_publications_with_venue_series
    WHERE
        venue_series_id = 2

GO

-- get all publications from VLDB (Journal)
CREATE VIEW dbo.view_acm_publications_vldb_journal
AS
    SELECT 
        id
       ,title
    FROM 
        dbo.view_acm_publications_with_venue_series
    WHERE
        venue_series_id = 5

GO     

-- get all publications from VLDB (Series)
CREATE VIEW dbo.view_acm_publications_vldb_series
AS
    SELECT 
        id
       ,title
    FROM 
        dbo.view_acm_publications_with_venue_series
    WHERE
        venue_series_id = 4
        
GO

-- get all publications from SIGMOD (Series)
CREATE VIEW dbo.view_acm_publications_sigmod_series
AS
    SELECT 
        id
       ,title
    FROM 
        dbo.view_acm_publications_with_venue_series
    WHERE
        venue_series_id = 3