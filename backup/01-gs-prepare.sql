USE [dwhprak01];

--> drop all mapping tables

IF OBJECT_ID('dbo.gs_author_publication_maps', 'U') IS NOT NULL
  DROP TABLE [dbo].[gs_author_publication_maps];
  
IF OBJECT_ID('dbo.gs_publication_venue_maps', 'U') IS NOT NULL
  DROP TABLE [dbo].[gs_publication_venue_maps];

--< drop all mapping tables

--> drop all entity tables

IF OBJECT_ID('dbo.gs_publications_import', 'U') IS NOT NULL
  DROP TABLE [dbo].[gs_publications_import];

IF OBJECT_ID('dbo.gs_authors', 'U') IS NOT NULL
  DROP TABLE [dbo].[gs_authors];

IF OBJECT_ID('dbo.gs_publications', 'U') IS NOT NULL
  DROP TABLE [dbo].[gs_publications];
 
IF OBJECT_ID('dbo.gs_venues', 'U') IS NOT NULL
  DROP TABLE [dbo].[gs_venues];

IF OBJECT_ID('dbo.gs_venue_series', 'U') IS NOT NULL
  DROP TABLE [dbo].[gs_venue_series];
  
IF OBJECT_ID('dbo.gs_origins', 'U') IS NOT NULL
  DROP TABLE [dbo].[gs_origins];

--< drop all entity tables
 
--> create all tables
  
CREATE TABLE [dbo].[gs_publications_import]
(
    id				numeric(20, 0)

   ,title			nvarchar(MAX)

   ,url				nvarchar(MAX)

   ,no_of_citings	int

   ,additional		nvarchar(MAX)
)

CREATE TABLE [dbo].[gs_authors]
(
    id      bigint
            CONSTRAINT gs_authors_id_nn NOT NULL
            CONSTRAINT gs_authors_pk PRIMARY KEY

   ,name	varchar(255)
);

CREATE TABLE [dbo].[gs_origins]
(
    id      bigint
            CONSTRAINT gs_origins_id_nn NOT NULL
            CONSTRAINT gs_origins_pk PRIMARY KEY

   ,name    varchar(255)
);

CREATE TABLE [dbo].[gs_publications]
(
    id				numeric(20, 0)
					CONSTRAINT gs_publications_id_nn NOT NULL
					CONSTRAINT gs_publications_pk PRIMARY KEY

   ,no_of_citings	int

   ,origin_id		bigint
                    -- CONSTRAINT gs_publications_origin_id_nn NOT NULL
                    CONSTRAINT gs_publications_origin_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[gs_origins] (id)
                        
   ,title			varchar(255)

   ,url				nvarchar(MAX)

);

CREATE TABLE [dbo].[gs_venue_series]
(
    id      bigint IDENTITY(1, 1)
            CONSTRAINT gs_venue_series_id_nn NOT NULL
            CONSTRAINT gs_venue_series_pk PRIMARY KEY

   ,name    varchar(255)
);

CREATE TABLE [dbo].[gs_venues]
(
    id              bigint
                    CONSTRAINT gs_venues_id_nn NOT NULL
                    CONSTRAINT gs_venues_pk PRIMARY KEY

   ,year            int

   ,venue_series_id bigint
                    -- there are some venues without a venue series
                    -- CONSTRAINT gs_venues_venue_series_id_nn NOT NULL
                    CONSTRAINT gs_venues_venue_series_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[gs_venue_series] (id)
);

CREATE TABLE [dbo].[gs_author_publication_maps]
(
    author_id       bigint
                    CONSTRAINT gs_author_publication_maps_author_id_nn NOT NULL
                    CONSTRAINT gs_author_publication_maps_author_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[gs_authors] (id)

   ,publication_id  numeric(20, 0)
                    CONSTRAINT gs_author_publication_maps_publication_id_nn NOT NULL
                    CONSTRAINT gs_author_publication_maps_publication_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[gs_publications] (id)

   ,position        tinyint

   ,CONSTRAINT gs_author_publication_maps_pk
        -- there are some authors listed more than once for the same publication
        PRIMARY KEY (publication_id, author_id, position)
);

CREATE TABLE [dbo].[gs_publication_venue_maps]
(
    publication_id  numeric(20, 0)
                    CONSTRAINT gs_publication_venue_maps_publication_id_nn NOT NULL
                    CONSTRAINT gs_publication_venue_maps_publication_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[gs_publications] (id)
 
   ,venue_id        bigint
                    CONSTRAINT gs_publication_venue_maps_venue_id_nn NOT NULL
                    CONSTRAINT gs_publication_venue_maps_venue_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[gs_venues] (id)

   ,CONSTRAINT gs_publication_venue_maps_pk 
        PRIMARY KEY (publication_id, venue_id)
);

--< create all tables
