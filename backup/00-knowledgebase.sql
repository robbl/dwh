-- rows to columns
SELECT
    @part1 = [1]
   ,@part2 = [2]
   ,@part3 = [3]
   ,@part4 = [4]
FROM
    (
        SELECT * FROM dbo.fn_split_into_rows(@additional, @delimiter, @trim)
    ) AS source_table
PIVOT
    (
        MAX(item) FOR id IN ([1], [2], [3], [4])
    ) AS pivot_table
;