USE [dwhprak01]

-- drop publication venue series mapping 
IF OBJECT_ID('dbo.dblp_publication_venue_series_maps_temp', 'U') IS NOT NULL
    DROP TABLE [dbo].[dblp_publication_venue_series_maps_temp];
