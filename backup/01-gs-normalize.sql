USE [dwhprak01];

--> clear all mapping tables

IF OBJECT_ID('dbo.gs_author_publication_maps', 'U') IS NOT NULL
  DELETE FROM [dbo].[gs_author_publication_maps];
  
IF OBJECT_ID('dbo.gs_publication_venue_maps', 'U') IS NOT NULL
  DELETE FROM [dbo].[gs_publication_venue_maps];

--< clear all mapping tables

--> clear all entity tables

IF OBJECT_ID('dbo.gs_authors', 'U') IS NOT NULL
  DELETE FROM [dbo].[gs_authors];

IF OBJECT_ID('dbo.gs_publications', 'U') IS NOT NULL
  DELETE FROM [dbo].[gs_publications];

IF OBJECT_ID('dbo.view_gs_venues', 'V') IS NOT NULL
  DROP VIEW [dbo].[view_gs_venues];

IF OBJECT_ID('dbo.gs_venues', 'U') IS NOT NULL
  DELETE FROM [dbo].[gs_venues];

IF OBJECT_ID('dbo.gs_venue_series', 'U') IS NOT NULL
  DELETE FROM [dbo].[gs_venue_series];
  
IF OBJECT_ID('dbo.gs_origins', 'U') IS NOT NULL
  DELETE FROM [dbo].[gs_origins];

--< clear all entity tables

-- insert all origins
INSERT INTO
	[dbo].[gs_origins]
	(
	    id
	   ,name
	) 
	SELECT
	   ROW_NUMBER() OVER(ORDER BY origin_name ASC)
	  ,origin_name
	FROM
		[dbo].[gs_publications_splitted_additional]
	WHERE
	    origin_name IS NOT NULL
	GROUP BY origin_name
;

-- insert all publications
INSERT INTO
	[dbo].[gs_publications]
	(
	    id
	   ,no_of_citings
	   ,origin_id
	   ,title
	   ,url
	) 
	SELECT DISTINCT
	   publications_splitted_additional.id
	  ,publications_splitted_additional.no_of_citings
	  ,origins.id AS origin_id
	  ,publications_splitted_additional.title
	  ,publications_splitted_additional.url
	FROM
		[dbo].[gs_publications_splitted_additional] AS publications_splitted_additional
	    LEFT JOIN 
	        [dbo].[gs_origins] 
	        AS 
	            origins 
	        ON 
	            (publications_splitted_additional.origin_name = origins.name)
;

-- insert all authors
INSERT INTO
	[dbo].[gs_authors]
	(
	    id
	   ,name
	) 
	SELECT
	    -- ROW_NUMBER trick, see 'insert all author publication mappings'
	    ROW_NUMBER() OVER(ORDER BY id, author_position ASC)
	   ,author_name
	FROM
		[dbo].[gs_publications_splitted_additional]
;

-- insert all author publication mappings
INSERT INTO 
    [dbo].[gs_author_publication_maps]
    (
        author_id
       ,position
       ,publication_id
    )
    SELECT
        -- using the same ROW_NUMER as before for simpler author_id handling 
        ROW_NUMBER() OVER(ORDER BY id, author_position ASC) AS author_id
       ,author_position
       ,id
    FROM
        [dbo].[gs_publications_splitted_additional]
;

-- insert all venue series
INSERT INTO
	[dbo].[gs_venue_series]
	(
	    name
	) 
	SELECT
	   venue_series_name
	FROM
		[dbo].[gs_publications_splitted_additional]
	WHERE
	    venue_series_name IS NOT NULL
	GROUP BY venue_series_name
;

GO

-- create a view for all venues (all publications with venue series name and/or year)
CREATE VIEW
     [dbo].[view_gs_venues]
AS
    SELECT
        ROW_NUMBER() OVER(ORDER BY publication_id ASC) AS venue_id
       ,dv.publication_id AS publication_id
       ,dv.venue_year AS venue_year
       ,vs.id AS venue_series_id
    FROM
        (
            SELECT DISTINCT
                id AS publication_id
               ,venue_year
               ,venue_series_name
            FROM
                [dbo].[gs_publications_splitted_additional]
        ) AS dv
        LEFT JOIN [dbo].[gs_venue_series] as vs
            ON dv.venue_series_name = vs.name
    WHERE 
        (
            dv.venue_year IS NOT NULL 
            OR
            dv.venue_series_name IS NOT NULL
        )
;

GO

-- insert all venues
INSERT INTO
    [dbo].[gs_venues]
    (
        id
       ,venue_series_id
       ,year
    )
    SELECT 
        venue_id
       ,venue_series_id
       ,venue_year
    FROM 
        [dbo].[view_gs_venues]
;

-- insert all publication venue mappings
INSERT INTO
    [dbo].[gs_publication_venue_maps]
    (
        publication_id
       ,venue_id
    )
    SELECT 
        publication_id
       ,venue_id
    FROM 
        [dbo].[view_gs_venues]
;
