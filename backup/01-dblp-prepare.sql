USE [dwhprak01];

--> drop all mapping tables

IF OBJECT_ID('dbo.dblp_author_publication_maps', 'U') IS NOT NULL
    DROP TABLE [dbo].[dblp_author_publication_maps];

IF OBJECT_ID('dbo.dblp_publication_venue_series_maps_temp', 'U') IS NOT NULL
    DROP TABLE [dbo].[dblp_publication_venue_series_maps_temp];

IF OBJECT_ID('dbo.dblp_publication_venue_maps', 'U') IS NOT NULL
    DROP TABLE [dbo].[dblp_publication_venue_maps];

--< drop all mapping tables

--> drop all entity tables

IF OBJECT_ID('dbo.dblp_authors', 'U') IS NOT NULL
    DROP TABLE [dblp_authors];

IF OBJECT_ID('dbo.dblp_publications', 'U') IS NOT NULL
    DROP TABLE [dbo].[dblp_publications];

IF OBJECT_ID('dbo.dblp_venues', 'U') IS NOT NULL
    DROP TABLE [dbo].[dblp_venues];

IF OBJECT_ID('dbo.dblp_venue_series', 'U') IS NOT NULL
    DROP TABLE [dbo].[dblp_venue_series];

--< drop all entity tables

--> create all tables

CREATE TABLE [dbo].[dblp_authors]
(
    id          bigint
                CONSTRAINT dblp_authors_id_nn NOT NULL
                CONSTRAINT dblp_authors_pk PRIMARY KEY

   ,first_name  varchar(255)

   ,name        varchar(255)

   ,last_name   varchar(255)

   ,url         varchar(255)
);

CREATE TABLE [dbo].[dblp_publications]
(
    id          bigint
                CONSTRAINT dblp_publications_id_nn NOT NULL
                CONSTRAINT dblp_publications_pk PRIMARY KEY

   ,dblp_code   varchar(255)

   ,end_page    bigint

   ,start_page  bigint

   ,title       varchar(255)

   ,year        int
);

CREATE TABLE [dbo].[dblp_venue_series]
(
    id      bigint
            CONSTRAINT dblp_venue_series_id_nn NOT NULL
            CONSTRAINT dblp_venue_series_pk PRIMARY KEY

   ,name    varchar(255)

   ,type    varchar(255)
);

CREATE TABLE [dbo].[dblp_venues]
(
    id              bigint
                    IDENTITY(0, 1)
                    CONSTRAINT dblp_venues_id_nn NOT NULL
                    CONSTRAINT dblp_venues_pk PRIMARY KEY

   ,venue_series_id bigint
                    CONSTRAINT dblp_venues_venue_series_id_nn NOT NULL
                    CONSTRAINT dblp_venues_venue_series_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[dblp_venue_series] (id)
);

CREATE TABLE [dbo].[dblp_author_publication_maps]
(
    author_id       bigint
                    CONSTRAINT dblp_author_publication_maps_author_id_nn NOT NULL
                    CONSTRAINT dblp_author_publication_maps_author_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[dblp_authors] (id)

   ,publication_id  bigint
                    CONSTRAINT dblp_author_publication_maps_publication_id_nn NOT NULL
                    CONSTRAINT dblp_author_publication_maps_publication_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[dblp_publications] (id)

   ,position        tinyint

   ,CONSTRAINT dblp_author_publication_maps_pk 
        PRIMARY KEY (publication_id, author_id, position)
);

CREATE TABLE [dbo].[dblp_publication_venue_series_maps_temp]
(
    venue_series_id bigint

   ,publication_id  bigint
);

CREATE TABLE [dbo].[dblp_publication_venue_maps]
(
    publication_id  bigint
                    CONSTRAINT dblp_publication_venue_maps_publication_id_nn NOT NULL
                    CONSTRAINT dblp_publication_venue_maps_publication_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[dblp_publications] (id)
 
   ,venue_id        bigint
                    CONSTRAINT dblp_publication_venue_maps_venue_id_nn NOT NULL
                    CONSTRAINT dblp_publication_venue_maps_venue_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[dblp_venues] (id)

   ,CONSTRAINT dblp_publication_venue_maps_pk 
        PRIMARY KEY (publication_id, venue_id)
);

--< create all tables
