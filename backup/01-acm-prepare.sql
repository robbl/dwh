USE [dwhprak01];

--> drop all temp tables

IF OBJECT_ID('dbo.acm_authors_temp', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_authors_temp];
  
IF OBJECT_ID('dbo.acm_citing_publications_temp', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_citing_publications_temp];

IF OBJECT_ID('dbo.acm_publications_temp', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_publications_temp];

IF OBJECT_ID('dbo.acm_venues_temp', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_venues_temp];

IF OBJECT_ID('dbo.acm_venue_series_temp', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_venue_series_temp];

--< drop all temp tables 

--> create all temp tables

CREATE TABLE [dbo].[acm_authors_temp] (
    position        tinyint
   ,name            nvarchar(255)
   ,institution     nvarchar(255)
   ,publication_id  bigint
)

CREATE TABLE  [dbo].[acm_citing_publications_temp] (
    id              bigint
   ,text            nvarchar(4000)
   ,publication_id  bigint
)

CREATE TABLE [dbo].[acm_publications_temp] (
    publication_id  bigint
   ,id              bigint
   ,title           nvarchar(1000)
   ,fulltext        nvarchar(255)
   ,start_page      int
   ,end_page        int
)

CREATE TABLE [dbo].[acm_venues_temp] (
    id              bigint
   ,year            int
   ,text            nvarchar(255)
   ,publication_id  bigint
)

CREATE TABLE [dbo].[acm_venue_series_temp] (
    id              nvarchar(255)
   ,text            nvarchar(255)
   ,publication_id  bigint
)

--< create all temp tables
