USE [dwhprak01]

--> drop all temp tables

IF OBJECT_ID('dbo.gs_publications_import', 'U') IS NOT NULL
    DROP TABLE [dbo].[gs_publications_import];

IF OBJECT_ID('dbo.gs_publications_splitted_additional', 'U') IS NOT NULL
    DROP TABLE [dbo].[gs_publications_splitted_additional];

--< drop ale temp tables

-- drop the venues view
IF OBJECT_ID('dbo.view_gs_venues', 'V') IS NOT NULL
    DROP VIEW [dbo].[view_gs_venues];
