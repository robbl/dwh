USE [dwhprak01];

GO

/*
    function will count the occurances of a substring in a string
    returns an int

    input:
     
        string   : 'stefan - robert - martin'
        substring: ' - '

    output:

        2
*/

IF OBJECT_ID('dbo.fn_count_occurances') IS NOT NULL
   DROP FUNCTION [dbo].[fn_count_occurances];

GO

CREATE FUNCTION [dbo].[fn_count_occurances] (@string nvarchar(max), @substring varchar(max))
RETURNS 
    INT
AS
BEGIN
    RETURN (DATALENGTH(REPLACE(@string, @substring, @substring + '#')) / 2 - DATALENGTH(@string) / 2)
END

GO

/*
    function will split a string by a given delimiter and optionally trims the resulting items
    returns a table like the following

    input:
     
        string   : 'stefan - robert'
        delimiter: ' - '
        trim     : 1

    output:

        id | item
        -----------
        1  | stefan
        2  | robert
*/

IF OBJECT_ID('dbo.fn_split_into_rows') IS NOT NULL
  DROP FUNCTION [dbo].[fn_split_into_rows];

GO

CREATE FUNCTION [dbo].[fn_split_into_rows] (@string nvarchar(max), @delimiter nvarchar(max), @trim bit)
RETURNS
    @result TABLE
    (
        id      int IDENTITY(1,1)
       ,item    nvarchar(max)
    )
AS
BEGIN
    DECLARE
        @occurances int
       ,@counter    int
       ,@part       nvarchar(max)

    SET @counter = 0;

    IF SUBSTRING(@string, DATALENGTH(@string) / 2, 1) <> @delimiter 
        SET @string = @string + @delimiter

    SET @occurances = dbo.fn_count_occurances(@string, @delimiter);

    WHILE @counter < @occurances
        BEGIN
            SET @part = SUBSTRING(@string, 0, CHARINDEX(@delimiter, @string));
            IF @trim = 1
                SET @part = LTRIM(RTRIM(@part))
            INSERT INTO @result(item)
                VALUES(@part);
        
            SET @string = SUBSTRING(@string, CHARINDEX(@delimiter, @string) + DATALENGTH(@delimiter) / 2, DATALENGTH(@string) / 2);
            SET @counter = @counter + 1;
        END
    RETURN 
END

GO

/*
    function will replace consecutive white spaces with a single white space

    input:
     
        string: '  a   a '

    output:

        ' a - a '
*/

IF OBJECT_ID('dbo.fn_strip_spaces') IS NOT NULL
  DROP FUNCTION [dbo].[fn_strip_spaces];

GO

CREATE FUNCTION [dbo].[fn_strip_spaces] (@string nvarchar(max))
RETURNS 
    nvarchar(max)
AS
BEGIN 
    WHILE CHARINDEX('  ', @string) > 0 
        SET @string = REPLACE(@string, '  ', ' ')

    RETURN @string
END