USE [dwhprak01];

GO

SELECT o.name,
  ddps.row_count 
FROM sys.indexes AS i
  INNER JOIN sys.objects AS o ON i.OBJECT_ID = o.OBJECT_ID
  INNER JOIN sys.dm_db_partition_stats AS ddps ON i.OBJECT_ID = ddps.OBJECT_ID
  AND i.index_id = ddps.index_id 
WHERE i.index_id < 2  AND o.is_ms_shipped = 0 ORDER BY o.NAME 

/*

name	                      row_count
---------------------------------------
acm_author_publication_maps	      14693
acm_authors	                       6896
acm_citings	                      50461
acm_fulltexts	                   5317
acm_institutions	               3975
acm_publication_venue_maps	       6724
acm_publications	               5516
acm_venue_series	                  6
acm_venues	                        356
dblp_author_publication_maps   	1960026
dblp_authors	                 501821
dblp_publication_venue_maps	     817108
dblp_publications	             818820
dblp_venue_series	               3901
dblp_venues	                       3901
gs_author_publication_maps	     314845
gs_authors	                     314845
gs_origins	                      15885
gs_publication_venue_maps	     103224
gs_publications	                 115246
gs_venue_series	                  47640
gs_venues	                     103224

*/