USE [dwhprak01];

--> clear all tables

IF OBJECT_ID('dbo.dblp_publication_venue_maps', 'U') IS NOT NULL
  DELETE FROM [dbo].[dblp_publication_venue_maps];
  
IF OBJECT_ID('dbo.dblp_venues', 'U') IS NOT NULL
  DELETE FROM [dbo].[dblp_venues];

--< clear all tables
  
-- insert a venue for each venue series
INSERT INTO 
    [dbo].[dblp_venues]
    (
        venue_series_id
    )
    SELECT
        venue_series.id AS venue_series_id
    FROM 
        [dbo].[dblp_venue_series] AS venue_series
    ORDER BY
        venue_series_id
;

-- insert a publication venue mapping from each publication venue series mapping
INSERT INTO
    [dbo].[dblp_publication_venue_maps]
    (
        venue_id
       ,publication_id
    )
    SELECT 
        venues.id                                           AS venue_id
       ,publication_venue_series_maps_temp.publication_id   AS publication_id
    FROM
        [dbo].[dblp_venues] AS venues
       ,[dbo].[dblp_publication_venue_series_maps_temp] AS publication_venue_series_maps_temp
    WHERE
        venues.venue_series_id = publication_venue_series_maps_temp.venue_series_id
;
