USE [dwhprak01];

IF OBJECT_ID('dbo.acm_institution_matches') IS NOT NULL 
    DROP TABLE dbo.acm_institution_matches

IF OBJECT_ID('dbo.acm_author_publication_institution_matches') IS NOT NULL 
    DROP TABLE dbo.acm_author_publication_institution_matches

IF OBJECT_ID('dbo.view_acm_author_publication_institution_mappings') IS NOT NULL 
    DROP VIEW [dbo].[view_acm_author_publication_institution_mappings]

IF OBJECT_ID('dbo.view_acm_institutions_cleaned') IS NOT NULL 
    DROP VIEW [dbo].[view_acm_institutions_cleaned]

IF OBJECT_ID('dbo.view_acm_author_publication_maps_cleaned') IS NOT NULL 
    DROP VIEW [dbo].[view_acm_author_publication_maps_cleaned]

IF OBJECT_ID('dbo.view_acm_institutions_normalized') IS NOT NULL 
    DROP VIEW [dbo].[view_acm_institutions_normalized]
    
IF OBJECT_ID('dbo.fn_replace_pattern') IS NOT NULL
    DROP FUNCTION [dbo].[fn_replace_pattern];

IF OBJECT_ID('dbo.fn_normalize_institution_name') IS NOT NULL
    DROP FUNCTION [dbo].[fn_normalize_institution_name];
    
CREATE TABLE dbo.acm_institution_matches
(
    id                  bigint

   ,name                nvarchar(255)

   ,name_clean          nvarchar(255)

   ,_key_in             int

   ,_key_out            int

   ,_score              real

   ,_Similarity_name    real
);

CREATE TABLE dbo.acm_author_publication_institution_matches
(
    left_institution_id     bigint

   ,left_institution_name   nvarchar(255)

   ,right_institution_id    bigint

   ,right_institution_name  nvarchar(255) 
);

GO

-- get all author, publication, institution mappings
CREATE VIEW dbo.view_acm_author_publication_institution_mappings
AS
    SELECT
        a.id AS author_id
       ,p.id AS publication_id
       ,v.year AS publication_year 
       ,i.id AS institution_id
       ,i.name AS institution_name
    FROM
        dbo.acm_authors AS a
    JOIN
        dbo.acm_author_publication_maps AS apm
        ON a.id = apm.author_id
    JOIN
        dbo.acm_publications AS p
        ON apm.publication_id = p.id
    JOIN
        dbo.acm_publication_venue_maps AS pvm
        ON p.id = pvm.publication_id
    JOIN
        dbo.acm_venues AS v
        ON pvm.venue_id = v.id
    JOIN
        dbo.acm_institutions AS i
        ON apm.institution_id = i.id

GO

CREATE FUNCTION [dbo].[fn_replace_pattern] (@pattern varchar(max), @antipattern varchar(max), @replacement varchar(max), @string nvarchar(max))
RETURNS 
    nvarchar(max)
AS
BEGIN
    DECLARE
        @start      int
       ,@stop       int
       ,@substring  nvarchar(max)
    
    SET @start = PATINDEX(@pattern, @string)
     
    WHILE @start > 0
    BEGIN
        SET @substring = SUBSTRING(@string, @start, LEN(@string))
        SET @stop = PATINDEX(@antipattern, @substring)
        IF @stop = 0
            SET @stop = LEN(@substring) + 1 
        SET @substring = SUBSTRING(@substring, 0, @stop)
        SET @string = REPLACE(@string, @substring, @replacement)
        SET @start = PATINDEX(@pattern, @string)
    END
    
    RETURN @string
END

GO

CREATE FUNCTION [dbo].[fn_normalize_institution_name] (@name nvarchar(max))
RETURNS 
    nvarchar(max)
AS
BEGIN
    -- replace numbers
    SET @name = dbo.fn_replace_pattern('%[0-9]%', '%[^0-9]%', '', @name)
    -- replace symbols
    SET @name = dbo.fn_replace_pattern('%[();,:./-\_{}]%', '%[^();,:./-\_{}]%', '', @name)
    -- replace double spaces
    SET @name = dbo.fn_strip_spaces(@name)
    
    SET @name = REPLACE(@name, 'Univ.', 'University')
    SET @name = REPLACE(@name, 'University of', 'University')
    SET @name = REPLACE(@name, 'Universitšt', 'University')
    
    SET @name = REPLACE(@name, 'Univ.', 'University')
    SET @name = REPLACE(@name, 'University of', 'University')
    SET @name = REPLACE(@name, 'Universitšt', 'University')    
    
    RETURN @name
END

GO

-- get all institutions that were selected for grouping others
CREATE VIEW dbo.view_acm_institutions_normalized
AS
    SELECT 
        i.id
       ,dbo.fn_normalize_institution_name(i.name) AS name
    FROM 
        dbo.acm_institutions AS i

GO

-- get all institutions that were selected for grouping others
CREATE VIEW dbo.view_acm_institutions_cleaned
AS
    SELECT 
        m.id
       ,m.name
    FROM 
        dbo.acm_institution_matches AS m
    WHERE
        m._key_in = m._key_out

GO

-- get all author publication maps with cleaned institutions
CREATE VIEW dbo.view_acm_author_publication_maps_cleaned
AS
    SELECT 
        apm.author_id
       ,apm.publication_id
       ,apm.position
       ,apm.institution_id
       ,(
            -- keep using old institution_id if there is no mapping
            CASE
            WHEN im._key_out IS NULL
            THEN apm.institution_id
            ELSE im._key_out
            END
        ) AS new_institution_id
    FROM 
        dbo.acm_author_publication_maps AS apm
    LEFT JOIN
        dbo.acm_institution_matches AS im
            ON apm.institution_id = im._key_in