USE [dwhprak01];

--> drop all tables with foreign keys

IF OBJECT_ID('dbo.acm_author_publication_maps', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_author_publication_maps];

IF OBJECT_ID('dbo.acm_publication_venue_maps', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_publication_venue_maps];

IF OBJECT_ID('dbo.acm_fulltexts', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_fulltexts];
  
IF OBJECT_ID('dbo.acm_citings', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_citings];

--< drop all tables with foreign keys

--> drop all entity tables
 
IF OBJECT_ID('dbo.acm_authors', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_authors];

IF OBJECT_ID('dbo.acm_publications', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_publications];
  
IF OBJECT_ID('dbo.acm_institutions', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_institutions];

IF OBJECT_ID('dbo.acm_venues', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_venues];
  
IF OBJECT_ID('dbo.acm_venue_series', 'U') IS NOT NULL
  DROP TABLE [dbo].[acm_venue_series];
  
IF OBJECT_ID('dbo.view_acm_venues', 'V') IS NOT NULL
  DROP VIEW [dbo].[view_acm_venues];

--< drop all entity tables

--> create all tables

CREATE TABLE [dbo].[acm_authors] (
    id      bigint IDENTITY(1, 1)
            CONSTRAINT acm_authors_id_nn NOT NULL
            CONSTRAINT acm_authors_pk PRIMARY KEY

   ,name    nvarchar(255)
)
  
CREATE TABLE [dbo].[acm_institutions] (
    id      bigint IDENTITY(1, 1)
            CONSTRAINT acm_institutions_id_nn NOT NULL
            CONSTRAINT acm_institutions_pk PRIMARY KEY

   ,name    nvarchar(255)
)
  
CREATE TABLE [dbo].[acm_publications] (
    id          bigint
                CONSTRAINT acm_publications_id_nn NOT NULL
                CONSTRAINT acm_publications_pk PRIMARY KEY

   ,end_page    int
   
   ,start_page  int
   
   ,title       nvarchar(1000)
)
  
CREATE TABLE [dbo].[acm_author_publication_maps] (
    author_id       bigint
                    CONSTRAINT acm_author_publication_maps_author_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[acm_authors] (id)

   ,publication_id  bigint
                    CONSTRAINT acm_author_publication_maps_publication_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[acm_publications] (id)

   ,institution_id  bigint
                    CONSTRAINT acm_author_publication_maps_institution_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[acm_institutions] (id)
                        
   ,position        tinyint
   
   ,CONSTRAINT acm_author_publication_maps_pk
        PRIMARY KEY (author_id, publication_id)
)

CREATE TABLE [dbo].[acm_venue_series] (
    id          bigint IDENTITY(1, 1)
                CONSTRAINT acm_venue_series_id_nn NOT NULL
                CONSTRAINT acm_venue_series_pk PRIMARY KEY

   ,acm_code    nvarchar(255)

   ,name        nvarchar(255)
)

CREATE TABLE [dbo].[acm_venues] (
    id                  bigint IDENTITY(1, 1)
                        CONSTRAINT acm_venues_id_nn NOT NULL
                        CONSTRAINT acm_venues_pk PRIMARY KEY

   ,name                nvarchar(255)

   ,year                int
   
   ,venue_series_id     bigint
                        CONSTRAINT acm_venues_venue_series_id_fk 
                            FOREIGN KEY REFERENCES [dbo].[acm_venue_series] (id)
)

CREATE TABLE [dbo].[acm_publication_venue_maps] (
    publication_id  bigint
                    CONSTRAINT acm_publication_venue_maps_publication_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[acm_publications] (id)

   ,venue_id        bigint
                    CONSTRAINT acm_publication_venue_maps_venue_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[acm_venues] (id)

   ,CONSTRAINT acm_publication_venue_maps_pk
        PRIMARY KEY (publication_id, venue_id)
)

CREATE TABLE [dbo].[acm_fulltexts] (
    id              bigint IDENTITY(1, 1)
                    CONSTRAINT acm_fulltexts_id_nn NOT NULL
                    CONSTRAINT acm_fulltexts_pk PRIMARY KEY

   ,fulltext        nvarchar(255)

   ,publication_id  bigint
                    CONSTRAINT acm_fulltexts_publication_id_fk 
                        FOREIGN KEY REFERENCES [dbo].[acm_publications] (id)
)

CREATE TABLE [dbo].[acm_citings] (
    citing_publication_id   bigint
                            CONSTRAINT acm_citings_citing_publication_id_fk 
                                FOREIGN KEY REFERENCES [dbo].[acm_publications] (id)

   ,cited_publication_id    bigint
                            -- there are publication cited, that do not exist
                            -- CONSTRAINT acm_citings_cited_publication_id_fk 
                            --    FOREIGN KEY REFERENCES [dbo].[acm_publications] (id)

   ,text                    nvarchar(4000)
   
   ,CONSTRAINT acm_citings_pk
        PRIMARY KEY (citing_publication_id, cited_publication_id)
)

--< create all tables

-- insert all authors
INSERT INTO
    [dbo].[acm_authors]
    (
        name
    )
    SELECT
        DISTINCT LTRIM(RTRIM(name)) AS name
    FROM
        [dbo].[acm_authors_temp]
;

-- insert all institutions
INSERT INTO
    [dbo].[acm_institutions]
    (
        name
    )
    SELECT
        DISTINCT LTRIM(RTRIM(institution)) AS name
    FROM
        [dbo].[acm_authors_temp]
;

-- insert all publications
INSERT INTO
    [dbo].[acm_publications]
    (
        id
       ,end_page
       ,start_page
       ,title
    )
    SELECT
        DISTINCT
        id
       ,end_page
       ,start_page
       ,LTRIM(RTRIM(title)) AS title
    FROM
        [dbo].[acm_publications_temp]
    ORDER BY id
;

-- insert all author publication mappings
INSERT INTO
    [dbo].[acm_author_publication_maps]
    (
        author_id
       ,publication_id
       ,institution_id
       ,position
    )
    SELECT
        DISTINCT
        authors.id AS author_id
       ,publications_temp.id AS publication_id
       ,institutions.id AS institution_id
       ,authors_temp.position AS position
    FROM
        [dbo].[acm_authors_temp] AS authors_temp
        LEFT JOIN [dbo].[acm_authors] AS authors
            ON LTRIM(RTRIM(authors_temp.name)) = authors.name
        LEFT JOIN [dbo].[acm_publications_temp] AS publications_temp
            ON authors_temp.publication_id = publications_temp.publication_id
        LEFT JOIN [dbo].[acm_institutions] AS institutions
            ON LTRIM(RTRIM(authors_temp.institution)) = institutions.name
;

-- insert all venue series
INSERT INTO
    [dbo].[acm_venue_series]
    (
        acm_code
       ,name
    )
    SELECT
        DISTINCT 
        id AS acm_code
       ,text AS name
    FROM 
        [dbo].[acm_venue_series_temp]
;

GO

-- create a view for venues temp with joined venue series and publications
CREATE VIEW
     [dbo].[view_acm_venues]
AS
    SELECT
        DISTINCT
        vt.id AS venue_id
       ,vt.text AS venue_name
       ,vt.year AS venue_year
       ,vs.id AS venue_series_id
       ,pt.id AS publication_id
    FROM
        [dbo].[acm_venues_temp] AS vt
        LEFT JOIN
            [dbo].[acm_venue_series_temp] as vst
            ON (
                vt.publication_id = vst.publication_id
            )
        LEFT JOIN
            [dbo].[acm_venue_series] as vs
            ON (
                vst.id = vs.acm_code
            )
        LEFT JOIN
            [dbo].[acm_publications_temp] as pt
            ON (
                vt.publication_id = pt.publication_id
            )
;

GO

-- insert all venues
INSERT INTO
    [dbo].[acm_venues]
    (
        name
       ,year
       ,venue_series_id
    )
    SELECT
        DISTINCT
        venue_name AS name
       ,venue_year AS year
       ,venue_series_id
    FROM
        [dbo].[view_acm_venues]
;

-- insert all publication venue mappings
INSERT INTO
    [dbo].[acm_publication_venue_maps]
    (
        publication_id
       ,venue_id
    )
    SELECT
        view_venues.publication_id AS publication_id
       ,venues.id AS venue_id
    FROM
        [dbo].[view_acm_venues] AS view_venues
        LEFT JOIN [dbo].[acm_venues] AS venues
            ON (
                view_venues.venue_series_id = venues.venue_series_id
                AND
                view_venues.venue_name = venues.name
                AND
                view_venues.venue_year = venues.year
            )
;

-- insert all fulltexts
INSERT INTO
    [dbo].[acm_fulltexts]
    (
        fulltext
       ,publication_id
    )
    SELECT
        fulltext
        ,id as publication_id
    FROM    
        [dbo].[acm_publications_temp]
    WHERE fulltext IS NOT NULL
;

-- insert all citings
INSERT INTO
    [dbo].[acm_citings]
    (
        cited_publication_id
       ,citing_publication_id
       ,text
    )
    SELECT
        DISTINCT
        cpt.id AS cited_publication_id
       ,pt.id AS citing_publication_id
       ,cpt.text
    FROM 
        [dbo].[acm_citing_publications_temp] AS cpt
        LEFT JOIN [dbo].[acm_publications_temp] AS pt
            ON cpt.publication_id = pt.publication_id
;
