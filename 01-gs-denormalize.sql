USE [dwhprak01];

GO

--> fn_gs_split_additional

/*
    function will split a google scholar additional string
    returns a table like the following 

    input:

        string   : 'JW Lottes, PF Fischer - Journal of Scientific Computing, 2005 - Springer'

    output:

        author     | venue                                 | origin
        ----------------------------------------------------------------
        JW Lottes  | Journal of Scientific Computing, 2005 | Springer
        PF Fischer | Journal of Scientific Computing, 2005 | Springer
*/

IF OBJECT_ID('dbo.fn_gs_split_additional') IS NOT NULL
  DROP FUNCTION [dbo].[fn_gs_split_additional];

GO

CREATE FUNCTION [dbo].[fn_gs_split_additional] 
    (
        @additional         nvarchar(max)
       ,@trim               bit
    )
RETURNS
    @result TABLE 
    (
        author_name         nvarchar(max)
       ,author_position     tinyint
       ,venue_series_name   nvarchar(max)
       ,venue_year          int
       ,origin_name         nvarchar(max)
    )
AS
BEGIN
    DECLARE
        @part1              nvarchar(max)
       ,@part2              nvarchar(max)
       ,@part3              nvarchar(max)
       ,@part4              nvarchar(max)
       ,@occurances         int
       ,@delimiter          nvarchar(max) = ' - '
       ,@author_names       nvarchar(max)
       ,@venue_series_name  nvarchar(max)
       ,@venue_year         int
       ,@origin_name        nvarchar(max)
    ;
    
    IF @trim = 1
        SET @additional = LTRIM(RTRIM(@additional))

    SET @occurances = dbo.fn_count_occurances(@additional, @delimiter);

    -- get all parts from the additional string by transforming the rows to columns and a single row
    SELECT
        @part1 = CASE id WHEN 1 THEN item ELSE @part1 END
       ,@part2 = CASE id WHEN 2 THEN item ELSE @part2 END
       ,@part3 = CASE id WHEN 3 THEN item ELSE @part3 END
       ,@part4 = CASE id WHEN 4 THEN item ELSE @part4 END
    FROM
        dbo.fn_split_into_rows(@additional, @delimiter, @trim)
    ;
    
    IF @trim = 1
        BEGIN
            SET @part1 = LTRIM(RTRIM(@part1))
            SET @part2 = LTRIM(RTRIM(@part2))
            SET @part3 = LTRIM(RTRIM(@part3))
            SET @part4 = LTRIM(RTRIM(@part4))
        END

    IF @occurances = 0
        SET @author_names = @part1;

    IF @occurances = 1
        BEGIN
            SET @author_names = @part1;

            -- if @part2 endes with a year
            IF PATINDEX('%, [0-9][0-9][0-9][0-9]', RTRIM(@part2)) > 0
                SET @venue_series_name = @part2;
            ELSE
                SET @origin_name = @part2;
        END

    IF @occurances = 2
        BEGIN
          SET @author_names         = @part1;
          SET @venue_series_name    = @part2;
          SET @origin_name     = @part3;
        END

    IF @occurances = 3
        BEGIN
            SET @author_names       = @part1;
            SET @venue_series_name  = @part2 + @delimiter + @part3;
            SET @origin_name   = @part4;
        END

    -- check for year in venue series name
    IF @venue_series_name IS NOT NULL
        BEGIN
            -- if it's only a year
            IF PATINDEX('[0-9][0-9][0-9][0-9]', @venue_series_name) = 1
                BEGIN
                    SET @venue_year         = CONVERT(int, @venue_series_name);
                    SET @venue_series_name  = NULL;
                END

            -- if it ends with an year
            IF PATINDEX('%, [0-9][0-9][0-9][0-9]', @venue_series_name) > 0
                BEGIN
                    SET @venue_year         = CONVERT(int, RIGHT(@venue_series_name, 4));
                    SET @venue_series_name  = LEFT(@venue_series_name, DATALENGTH(@venue_series_name) / 2 - 6);
                END
        END

    -- insert the data in the result table
    INSERT INTO @result 
        (
            author_name
           ,author_position
           ,venue_series_name
           ,venue_year
           ,origin_name
        ) 
        SELECT 
            authors.item
           ,authors.id
           ,@venue_series_name
           ,@venue_year
           ,@origin_name
        FROM 
            dbo.fn_split_into_rows(@author_names, ', ', @trim) AS authors
    ;

    RETURN
END

--< fn_gs_split_additional

GO

/*
    input:

        publication_id      | additional
        -------------------------------------------------------------------------------------------------------------------------
        9268399736994965182 | MJ Franklin, MJ Carey, M Livny - ACM Transactions on Database Systems (TODS), 1997 - portal.acm.org

    output in table @publication_author_maps_temp:

        id                  | author_name | author_position | venue_series_name                           | venue_year | origin_name
        -------------------------------------------------------------------------------------------------------------------------------
        9268399736994965182	| MJ Franklin | 1               | ACM Transactions on Database Systems (TODS) | 1997       | portal.acm.org
        9268399736994965182	| MJ Carey    | 2               | ACM Transactions on Database Systems (TODS) | 1997       | portal.acm.org
        9268399736994965182	| M Livny     | 3               | ACM Transactions on Database Systems (TODS) | 1997       | portal.acm.org
*/

IF OBJECT_ID('dbo.gs_publications_splitted_additional', 'U') IS NOT NULL
  DROP TABLE [dbo].[gs_publications_splitted_additional];
  
CREATE TABLE [dbo].[gs_publications_splitted_additional] 
(
     id                 numeric(20, 0)
    ,title              nvarchar(1000)
    ,url                nvarchar(1000)
    ,no_of_citings      int
    ,author_name        nvarchar(1000)
    ,author_position    tinyint
    ,venue_series_name  nvarchar(1000)
    ,venue_year         nvarchar(1000)
    ,origin_name        nvarchar(1000)
)

GO

INSERT INTO [dbo].[gs_publications_splitted_additional] 
    (
        id
       ,title
       ,url
       ,no_of_citings
       ,author_name
       ,author_position
       ,venue_series_name
       ,venue_year
       ,origin_name
    )
    SELECT
         publications.id
        ,publications.title
        ,publications.url
        ,publications.no_of_citings
        ,results.author_name
        ,results.author_position
        ,results.venue_series_name
        ,results.venue_year
        ,results.origin_name
    FROM
        [dbo].[gs_publications_import] AS publications
    OUTER APPLY
        dbo.fn_gs_split_additional(publications.additional, 1) AS results
;
