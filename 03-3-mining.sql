USE [dwhprak01];

-- drop views

IF OBJECT_ID('dbo.view_dblp_authors_with_venue_series') IS NOT NULL 
    DROP VIEW [dbo].[view_dblp_authors_with_venue_series];

GO

CREATE VIEW dbo.view_dblp_authors_with_venue_series
AS
    SELECT DISTINCT
        dblp_apm.author_id 
       ,dblp_vs.id AS venue_series_id
       ,dblp_vs.name AS venue_series_name    
    FROM
        dbo.dblp_author_publication_maps AS dblp_apm
    JOIN
        dbo.dblp_publication_venue_maps AS dblp_pvm
        ON dblp_apm.publication_id = dblp_pvm.publication_id
    JOIN
        dbo.dblp_venues AS dblp_v
        ON dblp_pvm.venue_id = dblp_v.id
    JOIN
        dbo.dblp_venue_series AS dblp_vs
        ON dblp_v.venue_series_id = dblp_vs.id
;