USE [dwhprak01];

IF OBJECT_ID('dbo.dblp_acm_publication_matches') IS NOT NULL 
    DROP TABLE dbo.dblp_acm_publication_matches

IF OBJECT_ID('dbo.view_dblp_acm_publication_matches') IS NOT NULL 
    DROP VIEW [dbo].[view_dblp_acm_publication_matches]
    
IF OBJECT_ID('dbo.dblp_gs_publication_matches') IS NOT NULL 
    DROP TABLE dbo.dblp_gs_publication_matches

IF OBJECT_ID('dbo.view_dblp_gs_publication_matches') IS NOT NULL 
    DROP VIEW [dbo].[view_dblp_gs_publication_matches]

CREATE TABLE dbo.dblp_acm_publication_matches
(
    dblp_id             bigint
        
   ,acm_id              bigint
       
   ,_Similarity         real
       
   ,_Confidence         real
      
   ,_Similarity_title   real
       
   ,CONSTRAINT dblp_acm_publication_matches_pk
        PRIMARY KEY (dblp_id, acm_id)
);

CREATE TABLE dbo.dblp_gs_publication_matches
(
    dblp_id             bigint
        
   ,gs_id               numeric(20, 0)
       
   ,_Similarity         real
       
   ,_Confidence         real
      
   ,_Similarity_title   real
       
   ,CONSTRAINT dblp_gs_publication_matches_pk
        PRIMARY KEY (dblp_id, gs_id)
);

GO

-- get all matched publications (dblp<->acm) with titles
CREATE VIEW dbo.view_dblp_acm_publication_matches
AS
    SELECT 
        m.dblp_id
       ,m.acm_id
       ,m._Similarity_title
       ,dblp_p.title AS dblp_title
       ,acm_p.title AS acm_title
    FROM 
        dbo.dblp_acm_publication_matches AS m
    JOIN
        dbo.dblp_publications AS dblp_p 
            ON m.dblp_id = dblp_p.id
    JOIN
        dbo.acm_publications AS acm_p 
            ON m.acm_id = acm_p.id

GO

-- get all matched publications (dblp<->gs) with titles
CREATE VIEW dbo.view_dblp_gs_publication_matches
AS
    SELECT 
        m.dblp_id
       ,m.gs_id
       ,m._Similarity_title
       ,dblp_p.title AS dblp_title
       ,gs_p.title AS gs_title
    FROM 
        dbo.dblp_gs_publication_matches AS m
    JOIN
        dbo.dblp_publications AS dblp_p 
            ON m.dblp_id = dblp_p.id
    JOIN
        dbo.gs_publications AS gs_p 
            ON m.gs_id = gs_p.id